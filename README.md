# Divante front-end developer recruitment test


## Quick view:
Just check dist folder

## Requirements for watch & build

  * [Node.js](http://nodejs.org)
* [gulp](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md#getting-started) `npm install --global gulp`
  * [bower](http://bower.io): `npm install bower -g`

## Quickstart

* Run `npm install`
* Run `bower install`
  
When you're working on your project, just run the following command:

```bash
gulp watch
```

or to build a dist just run:

```bash
gulp
```