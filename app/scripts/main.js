$(function(){

	// http://www.owlgraphic.com/owlcarousel/
	$(".js-carousel1").owlCarousel({
        singleItem: true
	});

	$(".js-carousel2").owlCarousel({
	    singleItem: true
	});

	// show side nav
	$('.js-show-nav').click(function(e){
		e.preventDefault();
		e.stopPropagation();
		$(this).toggleClass('active');
		$('body').toggleClass('show-side-nav');
	});

	// prevent closing side nav after click
	$('.main-nav').click(function(e){
		e.stopPropagation();
	});

	// hide side nav after body click
	$('body').click(function(){
		if ($(this).hasClass('show-side-nav')) {
			$(this).removeClass('show-side-nav');
		}
	});
});